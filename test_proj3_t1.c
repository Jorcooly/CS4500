#include <linux/unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <stdio.h>

#define __NR_virtual_space 341

int main(int argc, char *argv[]){
	int p_id;
	printf("Enter pid:");
	scanf("%d", &p_id);
	syscall(__NR_virtual_space, p_id); 
	return 0;
}

