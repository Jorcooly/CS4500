#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/pid.h>

asmlinkage int sys_print_other(int p_id){
	struct task_struct* task;
	struct pid* pid_struct;
	
	pid_struct = find_get_pid(p_id);
	task = pid_task(pid_struct, PIDTYPE_PID);

	printk(KERN_EMERG "%d task information: ", p_id);
	printk(KERN_EMERG "%s %d %ld\n", task->comm, task->pid, task->state);

	printk(KERN_EMERG "Printing task list:\n");
	for(ptask=task;ptast!=&init_task;ptask=ptask->parent){
		printk(KERN_EMERG "%s %d\n", ptask->comm, ptask->pid);
	}
	return 0;
}
