#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/mm_types.h>
#include <linux/mm.h>
#include <linux/highmem.h>
#include <asm/page.h>
#include <asm/pgtable.h>

asmlinkage int sys_proc_status(unsigned long mem, int p_id){
	spinlock_t *lock;
	int present_mem = 0;	
	pte_t *ptep;
	pte_t pte;

	struct mm_struct *vma = find_task_by_vpid(p_id)->active_mm;
	

	pgd_t *pgd = pgd_offset(vma, mem);
	pud_t *pud = pud_offset(pgd, mem);
	pmd_t *pmd = pmd_offset(pud, mem);
	
	ptep = pte_offset_map_lock(vma, pmd, mem, &lock);
	pte = *ptep;
	present_mem = pte_present(pte);
	printk("Data in memory?: %s\n", present_mem?"yes":"no");
	pte_unmap_unlock(ptep, lock);
	return 0;
}
