#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/module.h>

asmlinkage int sys_print_self(void){
	struct task_struct *task;
  
	//current->comm is the name of the executable
	//current->state is the running state
	//current->pid is the pid number

	printk(KERN_EMERG "Current task information: ");
	printk(KERN_EMERG "%s [%d] [%ld]\n", current->comm, current->pid, current->state);
	
	//Trace back to init. from website linked in assignment
	printk(KERN_EMERG "Printing task list:\n");
	for(task=current; task!=&init_task; task=task->parent){
		printk(KERN_EMERG "%s [%d]\n", task->comm, task->pid);
	}
	return 0;
}
