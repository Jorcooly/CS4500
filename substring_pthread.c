#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

#define MAX 1024
#define NUM_THREADS 2

int total = 0;
int n1, n2;
char *s1, *s2;
FILE *fp;

int read(FILE *fp){
	if((fp=fopen("strings.txt", "r")) == NULL){
		printf("ERROR: cannot open string.txt\n");
		return 0;
	}
	if((s1=(char*)malloc(sizeof(char)*MAX)) == NULL){
		printf("ERROR: out of memory\n");
		return -1;
	}
	if((s2=(char*)malloc(sizeof(char)*MAX)) == NULL){
		printf("ERROR: out of memory\n");
		return -1;
	}
	s1=fgets(s1, MAX, fp);
	s2=fgets(s2, MAX, fp);
	n1=strlen(s1);
	n2=strlen(s2)-1;
	if(s1 == NULL || s2 == NULL || n1<n2)
		return -1;	
}

void *num_substring_threaded(void *t){
	int offset;
	int count = 0;
	int substrings = 0;
	int i, j, k;
	offset = (int)t;
printf("This is thread %d\n", offset);
	for(i = 0; i <= (n1-n2); i++){
		count = 0;
		if((i-offset)%NUM_THREADS == 0){
			for(j = i, k = 0; k < n2 ;j++, k++){
				if(*(s1+j) != *(s2+k))
					break;
				else
					count++;
					
				if(count == n2)
					total++;
			}
		}
	}
}

int main(){
	pthread_t thread[NUM_THREADS];
	read(fp);
	int i;
	for(i=0; i<NUM_THREADS; i++){
		pthread_create(&thread[i], NULL, num_substring_threaded,
			(void *)i);
		pthread_join(thread[i], NULL);
	}
	printf("The number of substrings is: %d\n", total);
	return 0;
}
