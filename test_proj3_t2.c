#include <linux/unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <stdio.h>

#define __NR_proc_status 340

int main(int argc, char *argv[]){
	int p_id;
	printf("Enter pid:");
	scanf("%d", &p_id);
	unsigned long mem;
	printf("Enter memory addr:");
	scanf("%ld", &mem);
	syscall(__NR_proc_status, mem, p_id); 
	return 0;
}

