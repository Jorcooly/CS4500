#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/mm_types.h>
#include <linux/mm.h>
#include <linux/fs.h>
#include <asm/page.h>
#include <asm/mman.h>

asmlinkage int sys_virtual_space(int p_id){
	struct task_struct *p_task;
	struct mm_struct *this_mm;
	struct vm_area_struct *all_vmas;
	struct vm_area_struct p_vma;
	
	int vmas_tot;
	int count = 0;
	int total_size = 0;
	
	p_task = find_task_by_vpid(p_id);
	this_mm = p_task->active_mm;
	all_vmas = this_mm->mmap;
	vmas_tot = this_mm->map_count;

	for(p_vma = *all_vmas; count < vmas_tot; count++){
		total_size += (p_vma.vm_end - p_vma.vm_start);
		if(count != vmas_tot-1)
			p_vma = *(p_vma.vm_next);
	}
	printk("VMA total size for pid%d: %d\n", p_id, total_size);
	return 0;
}
